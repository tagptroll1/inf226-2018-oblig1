package inf226;
import java.lang.Character;
import java.util.Date;

public class Message {
	public final String sender, recipient, message;
	public final Date timestamp;


	public Message(final String user, final String recipient, final Date timestamp, final String message)
			throws Invalid {
		this.sender = user;
		this.recipient = recipient;
		if (!valid(message)){
			throw new Invalid(message);
		}
		this.timestamp = timestamp;
		this.message = message;
	}

	Message(final User user, final String recipient, final Date timestamp, final String message)
		throws Invalid {
		this.sender = user.getName();
		this.recipient = recipient;
		if (!valid(message)){
			throw new Invalid(message);
		}
		this.timestamp = timestamp;
		this.message = message;
	}

	public static boolean valid(String message) {
		if (message.equals(".")) return false;

		String newMsg = message.replace("\n", "");
		char[] chars = newMsg.toCharArray();
		for (char chr:chars){
			if (Character.isISOControl(chr)){
				return false;
			}
		}
		return true;
	}

	public static class Invalid extends Exception {
		private static final long serialVersionUID = -3451435075806445718L;

		public Invalid(String msg) {
			super("Invalid string: " + msg);
		}
	}
}
