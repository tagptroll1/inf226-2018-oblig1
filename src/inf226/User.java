package inf226;

import inf226.database.MsgBase;
import inf226.database.UserBase;

import java.util.ArrayList;

/**
 * Immutable class for users.
 * @author INF226
 *
 */
public final class User{
	
	private final Username name;
	private final ImmutableLinkedList<Message> log;
	private Token session;

	public User(final Username name) {
		this.name=name;

		ArrayList<Message> messages = MsgBase.getMessages(this.name);
		ImmutableLinkedList<Message> tmpList = new ImmutableLinkedList<>();

		for (Message m: messages){
			tmpList = new ImmutableLinkedList<>(m, tmpList);
		}
		this.log = tmpList;
	}

	private User(final Username name, final ImmutableLinkedList<Message> log) {
		this.name=name;
		this.log = log;
	}
	
	/**
	 * 
	 * @return User name
	 */
	public String getName() {
		return name.getUsername();
	}

    public Username getUsername(){return this.name;}

	boolean checkPassword(UserBase db, Hash password){
		return db.checkPassword(this.name, password);
	}

	/**
	 * @return Messages sent to this user.
	 */
	Iterable<Message> getMessages() {

		return log;
	}

	Token getSession() {
		return session;
	}

	void setSession(Token session) {
		this.session = session;
	}

	/**
	 * Add a message to this user’s log.
	 * @param m Message
	 * @return Updated user object.
	 */
	public User addMessage(Message m) {
		return new User(name,new ImmutableLinkedList<Message>(m,log));
	}

}
