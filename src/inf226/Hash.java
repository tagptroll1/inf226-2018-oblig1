package inf226;

import inf226.exceptions.IllegalPassword;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.logging.Logger;

public final class Hash {
    private final static SecureRandom random = new SecureRandom();
    private final String pass;
    private final String salt;

    Hash(String s) throws IllegalPassword {
        validate(s);
        this.salt = salt();
        this.pass = sha256(s, this.salt);
    }

    Hash(String p, String s) throws  IllegalPassword{
        validate(p);
        this.salt = s;
        this.pass = sha256(p, this.salt);
    }

    private void validate(String s) throws IllegalPassword{
        if (s.length() < 8 || s.length() > 40) throw new IllegalPassword();
        String pattern = "[a-zA-Z0-9.,:;()\\[\\]{}<>\"'#!$%&/+*?=\\-_|]{8,40}";
        boolean match = s.matches(pattern);

        if (!match) throw new IllegalPassword();
    }

    public String getPassword(){
        return this.pass;
    }

    public String getSalt(){
        return this.salt;
    }

    private static String sha256(String originalString, String salt){
        originalString = originalString + salt;
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] encodedHash = digest.digest(
                    originalString.getBytes(StandardCharsets.UTF_8));

            return bytesToHex(encodedHash);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            Logger.getLogger(Hash.class.getName()).warning(e.toString());
        }
        throw new NullPointerException();

    }

    private static String salt(){
        byte[] byteSalt = new byte[16];
        random.nextBytes(byteSalt);
        return bytesToHex(byteSalt);
    }


    private static String bytesToHex(byte[] hash){
        StringBuilder hexString = new StringBuilder();

        for(byte chr:hash){
            String hex = Integer.toHexString(0xff&chr);
            if(hex.length() == 1) hexString.append("0");
            hexString.append(hex);
        }
        return hexString.toString();
    }
}
