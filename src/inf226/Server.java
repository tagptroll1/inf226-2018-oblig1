package inf226;

import java.io.*;
import java.net.*;
import java.util.Date;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

import inf226.Storage.KeyedStorage;
import inf226.Storage.Storage;
import inf226.Storage.Storage.ObjectDeletedException;
import inf226.Storage.Stored;
import inf226.Storage.TransientStorage;
import inf226.database.MsgBase;
import inf226.database.UserBase;



/**
 * 
 * The Server main class. This implements all critical server functions.
 * 
 * @author INF226
 *
 */
public class Server {
	private static final Logger LOGGER = Logger.getLogger(Server.class.getName());
	private static UserBase<String, User> userDb = new UserBase<>();
	private static MsgBase<String, Message> msgBase = new MsgBase<>();
	private static final KeyedStorage<String,User> storage
	  = new TransientStorage<String, User>(User::getName);
	
	static Maybe<Stored<User>> authenticate(final Username username, final Hash password) {
		try {
		    LOGGER.info("authenticating with password");
			User user = new User(username);
			if (user.checkPassword(userDb, password)){
			    user.setSession(createToken().force());
				Maybe<Stored<User>> tmp = Maybe.just(storage.save(user));
				LOGGER.info("Authentication successful");
				return tmp;
			}
		}catch (IOException | Maybe.NothingException ex){
		        LOGGER.warning("Authentication failed " + ex);
				ex.printStackTrace();
		}


		return Maybe.nothing();
	}

	static Maybe<Stored<User>> register(final Username username, final Hash hash) {
		User user = new User(username);
		try{
            LOGGER.info("Attempting to register user ");
            user.setSession(createToken().force());
            Maybe<Stored<User>> tmp =  Maybe.just(userDb.save(user, hash));
            LOGGER.info("Registration successful");
            return tmp;
		} catch (IOException | Maybe.NothingException e){
		    LOGGER.warning("Exception in server register " + e);
			return Maybe.nothing();
		}
	}
	
	private static Maybe<Token> createToken() {
		return Maybe.just(new Token());
	}

	static Maybe<Stored<User>> authenticate(final Username username, final Token token) {
	    LOGGER.info("Authenticating with token.");
        Maybe<Stored<User>> serverUser = storage.lookup(username.getUsername());
        if (serverUser.isNothing()){
            LOGGER.info("User was not in memory, no valid session to fetch");
            return Maybe.nothing();
        } else {
            try {
                if (verifyToken(serverUser.force().getValue(), token)) {
                    LOGGER.info("Found user in memory, with matching token.  Access granted");
                    return Maybe.just(storage.save(serverUser.force().getValue()));
                }
            } catch (Maybe.NothingException | IOException e){
                e.printStackTrace();
                LOGGER.warning("Exception in token authentication " + e);
            }
            LOGGER.info("Entry rejected with token");
            return Maybe.nothing();
        }
	}

	private static boolean verifyToken(User user, Token token){
	    LOGGER.info("Verifying token for ");
	    Token userToken = user.getSession();
	    Date created = userToken.getCreated();
	    Date used = userToken.getLastUsed();
	    Date now = new Date();

	    long diff = now.getTime() - created.getTime();
	    long diffUsed = now.getTime() - used.getTime();
        int diffHours = (int) (diff / (60 * 60 * 1000));
        int diffHoursUsed = (int) (diffUsed / (60 * 60 * 1000));

	    if (diffHours > 24 || diffHoursUsed > 1){
            LOGGER.info("Token too old, not valid");
	        return false;
        }

        if (userToken.stringRepresentation().equals(token.stringRepresentation())){
            userToken.setLastUsed(new Date());
            LOGGER.info("Token valid, accepting entry.");
            return true;
        }
        LOGGER.info("Token invalid");
        return false;
    }

	static Maybe<String> validateUsername(String username){
		// This validation happens in Username now
		return Maybe.nothing();
	}

	static Maybe<String> validatePassword(String pass){
		// This validation happens in Hash now
		return Maybe.nothing();
	}

	static boolean sendMessage(Stored<User> sender, Message message) {
	    LOGGER.info("Got a send message request");
        if (!sender.getValue().getName().equals(message.sender)){
            return false;
        }
        User refreshed = sender.getValue().addMessage(message);
        try {
            storage.update(sender, refreshed);
        } catch (ObjectDeletedException | Storage.ObjectModifiedException | IOException e) {
            e.printStackTrace();
        }

        LOGGER.info("Saving message to database");
		msgBase.save(message);
		return true;
	}
	
	/**
	 * Refresh the stored user object from the storage.
	 * @param user user instance to be refreshed
	 * @return Refreshed value. Nothing if the object was deleted.
	 */
	static Maybe<Stored<User>> refresh(Stored<User> user) {
		try {
			return Maybe.just(storage.update(user, user.getValue()));
		} catch (ObjectDeletedException | Storage.ObjectModifiedException | IOException e) {
            return Maybe.nothing();
        }
	}
	
	/**
	 * @param args named argument: port-port to listen to
	 */
	public static void main(String[] args) throws IOException {
		final FileHandler fileHandler = new FileHandler("server.log");
		LOGGER.setLevel(Level.ALL);
		LOGGER.addHandler(fileHandler);

		String arg = System.getProperty("port", "1337");
		final int port = Integer.parseInt(arg);

        final RequestProcessor processor = new RequestProcessor();

		System.out.println("Staring authentication server on port "+port);
		LOGGER.info("Starting authenticating server on port "+port);
		processor.start();
		try (final ServerSocket socket = new ServerSocket(port)) {
            while(!socket.isClosed()) {
            	System.err.println("Waiting for client to connect…");
            	LOGGER.info("Waiting for client to connect...");
        		Socket client = socket.accept();
            	System.err.println("Client connected.");
            	LOGGER.info("Client connected: "+client.toString());
        		processor.addRequest(new RequestProcessor.Request(client));
			}
		} catch (IOException e) {
			System.out.println("Could not listen on port " + port);
			e.printStackTrace();
			LOGGER.warning("Got an exception while listening for connections: " + e);
		}
	}


    static Maybe<String> validateRecipient(String recipient) {

        return userDb.lookupUser(recipient);
	}
}
