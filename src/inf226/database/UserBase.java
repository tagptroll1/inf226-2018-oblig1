package inf226.database;

import inf226.Hash;
import inf226.Maybe;
import inf226.Storage.Id;
import inf226.Storage.Stored;
import inf226.Username;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserBase<K, User> extends BaseDatabase<K, User> {
    private final Id.Generator id_generator;

    public UserBase() {
        id_generator = new Id.Generator();
        try {
            // Attempt to connect to generate database
            connect();
            ensureTablesExist();
        } catch (FailedToConnectException e) {
            e.printStackTrace();
        }
    }


    public Maybe<String> lookupUser(String key) {
        String sql = "SELECT username FROM users WHERE username = ?;";

        try(Connection conn = connect();
            PreparedStatement prp = conn.prepareStatement(sql)){
            prp.setString(1, key);

            ResultSet result = prp.executeQuery();
            result.next();

            return Maybe.just(result.getString(1));
        } catch (SQLException | FailedToConnectException e){
            e.printStackTrace();
        }
        return Maybe.nothing();
    }

    /**
     * Saves a username
     *
     * @param value user instance which stores name to be saved
     * @param hash hash hash to be saved
     * @return a stored User
     * @throws IOException throws thing
     */

    public Stored<User> save(User value, final Hash hash) throws IOException {
        String SQL =
                "INSERT OR FAIL INTO users(username, password, salt)"
                        + "VALUES (?, ?, ?);";
        try(Connection conn = connect();
            PreparedStatement prp = conn.prepareStatement(SQL)){
            inf226.User user = (inf226.User) value;
            prp.setString(1, user.getName());
            prp.setString(2, hash.getPassword());
            prp.setString(3, hash.getSalt());

            prp.execute();
            return new Stored<>(id_generator, value);
        } catch (FailedToConnectException | SQLException e){
            System.out.println("User duplication");
            // To follow signature of overwrite, not optimal
            throw new IOException("Username taken");
        }

    }

    public static String getSalt(String username){
        String SQL =
                "SELECT salt FROM users where username = ?;";

        try(Connection conn = connect();
            PreparedStatement prp = conn.prepareStatement(SQL)){
            prp.setString(1, username);
            ResultSet result = prp.executeQuery();
            result.next();

            return result.getString("salt");
        } catch (FailedToConnectException | SQLException q){
            q.printStackTrace();
        }
        return "";
    }


    @Override
    public Stored<User> refresh(Stored<User> old) throws ObjectDeletedException, IOException {
        return super.refresh(old);
    }

    @Override
    public Stored<User> update(Stored<User> old, User newValue) throws ObjectModifiedException, ObjectDeletedException, IOException {
        return super.update(old, newValue);
    }

    @Override
    public void delete(Stored<User> old) throws ObjectModifiedException, ObjectDeletedException, IOException {
        super.delete(old);
    }



    public boolean checkPassword(Username username, Hash password){
        String query =
                "SELECT password FROM users "
                + "WHERE username = ?;";

        try(Connection conn = connect();
            PreparedStatement prp = conn.prepareStatement(query)){

            prp.setString(1, username.getUsername());
            ResultSet rs = prp.executeQuery();

            rs.next();
            String pass = rs.getString(1);
            return pass.equals(password.getPassword());

        } catch (FailedToConnectException | SQLException e){
            e.printStackTrace();
        }
        return false;
    }

}
