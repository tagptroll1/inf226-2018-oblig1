package inf226.database;

import inf226.Message;
import inf226.Storage.Id;
import inf226.Storage.Stored;
import inf226.Username;

import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Logger;

public class MsgBase<K, msg> extends BaseDatabase {
    private final Id.Generator id_generator;

    public MsgBase() {
        id_generator = new Id.Generator();
        try {
            connect();
            ensureTablesExist();
        } catch (FailedToConnectException e) {
            e.printStackTrace();
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public Stored<msg> save(Object value) {
        Message msg = (Message) value;

        String sql = "INSERT INTO messages(recipient, username, time, content) "
                + "values (?, ?, ?, ?);";
        try (Connection conn = connect();
             PreparedStatement prp = conn.prepareStatement(sql)){
            Timestamp timestamp = new java.sql.Timestamp(msg.timestamp.getTime());
            prp.setString(1, msg.recipient);
            prp.setString(2, msg.sender);
            prp.setTimestamp(3, timestamp);
            prp.setString(4, msg.message);

            prp.execute();
        } catch (FailedToConnectException | SQLException e){
            e.printStackTrace();
        }


        return new Stored<>(id_generator, (msg) value);


    }


    public static ArrayList<Message> getMessages(Username username){
        ArrayList<Message> messages = new ArrayList<>();
        String SQL =
            "SELECT recipient, username, time, content FROM messages where recipient = ?;";
        try(Connection conn = connect();
            PreparedStatement prp = conn.prepareStatement(SQL)){
            prp.setString(1, username.getUsername());
            ResultSet result = prp.executeQuery();


            while(result.next()){
                String recipient = result.getString("recipient");
                String sender = result.getString("username");
                Timestamp sent = result.getTimestamp("time");
                String content = result.getString("content");
                Message msg = new Message(recipient, sender, new Date(sent.getTime()), content);
                messages.add(msg);
            }
        } catch (FailedToConnectException | SQLException e){
            e.printStackTrace();
            Logger.getLogger(MsgBase.class.getName()).warning(e.toString());
        } catch (Message.Invalid invalid) {
            invalid.printStackTrace();
        }
        return messages;
    }
}
