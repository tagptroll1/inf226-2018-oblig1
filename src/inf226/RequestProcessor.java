package inf226;

import java.io.BufferedReader;

import inf226.Maybe.NothingException;
import inf226.Storage.Stored;
import inf226.database.UserBase;
import inf226.exceptions.IllegalPassword;
import inf226.exceptions.IllegalUsername;
import javafx.util.Pair;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class handles the requests from clients.
 * 
 * @author INF226
 *
 */
public final class RequestProcessor extends Thread {
	private static final Logger LOGGER = Logger.getLogger(RequestProcessor.class.getName());
	private final BlockingQueue<Request> queue;
	private HashMap<InetAddress, Pair<Date, ArrayList<Socket>>> monitor = new HashMap<>();


	public static void main(String[] args) throws IOException {
		final FileHandler fileHandler = new FileHandler("server.log");
		LOGGER.setLevel(Level.INFO);
		LOGGER.addHandler(fileHandler);
	}
	public RequestProcessor() {
		queue = new LinkedBlockingQueue<>();
	}

	/**
	 * Add a request to the queue.
	 * @param request a request to be queued
	 * @return boolean
	 */
	boolean addRequest(final Request request) {
		return queue.add(request);
	}
	
	public void run() {
		try {
			while(true) {
				final Request request = queue.take();
				InetAddress address = request.client.getInetAddress();
                LOGGER.info("Got request from "+ address.toString());

				checkMonitor(address);

				if (monitor.containsKey(address)){

					ArrayList<Socket> socks = monitor.get(address).getValue();

					if (socks.size() > 5){
                        LOGGER.warning("More than 5 requests from "+address.toString() + " ignoring this one");
						continue;
					} else {
						monitor.get(address).getValue().add(request.client);
					}
				} else {
					ArrayList<Socket> list = new ArrayList<>();
					list.add(request.client);
					monitor.put(address, new Pair<>(new Date(), list));
					LOGGER.info("Request added");
				}
                LOGGER.info("Starting request");
				request.start();
			}
		} catch (InterruptedException e) {
		    LOGGER.warning("Exception in Thread.run " + e);
			e.printStackTrace();
		}
	}
	private void checkMonitor(InetAddress addr){
	    if (!monitor.containsKey(addr)) return;

		Date date = monitor.get(addr).getKey();
		ArrayList<Socket> socks = monitor.get(addr).getValue();

		if (socks.size() > 5){
			if (Math.abs(date.getTime() - new Date().getTime()) / 1000 * 60 > 5){
				// If the difference between last request and now is more than 5 minutes
				// Clear all closed socks
				for (int i = socks.size()-1; i >= 0; i--) {
					if (socks.get(i).isClosed()){
					    LOGGER.info("Removed closed socket from list");
						socks.remove(socks.get(i));
					}
				}
			}
		}
	}

	/**
	 * The type of requests.
	 * @author INF226
	 *
	 */
	public static final class Request extends Thread{
		private final Socket client;
		private Maybe<Stored<User>> user;
		
		/**
		 * Create a new request from a socket connection to a client.
		 * @param client Socket to communicate with the client.
		 */
		public Request(final Socket client) {
			this.client = client;
			user = Maybe.nothing();
		}


		@Override
		public void run() {

		    try(final BufferedWriter out =
		            new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
		        final BufferedReader in = new BufferedReader(
		            new InputStreamReader(client.getInputStream()))) {
		    	while(true) {
		    		handle(in,out);
		    	}
		    } catch (IOException e) {
		    	// Client disconnected
                LOGGER.warning("Client disconnected "+ e);

		    }
			try {
				client.close();
			} catch (IOException e) {
				// Client closed.
			    LOGGER.warning("Client closed " + e);
			}
		}
		

		/**
		 * Handle a single request
		 * @param in Input from the client.
		 * @param out Output to the client.
		 * @throws IOException If the user hangs up unexpectedly
		 */
		private void handle(final BufferedReader in, final BufferedWriter out) throws IOException {
	    	final String requestType = Util.getLine(in);
	    	System.err.println("Request type: " + requestType);
            LOGGER.info("Handling request");
	    	if(requestType.equals("REGISTER")) {
	    		System.err.println("Handling registration request");
	    		LOGGER.info("Handling registration request");
	    		user = handleRegistration(in);
	    		try {
					out.write("REGISTERED " + user.force().getValue().getName()); out.newLine();
		    		System.err.println("Registration request succeeded.");
                    LOGGER.info("Registration request succeeded.");
                    out.write("TOKEN " + user.force().getValue().getSession().stringRepresentation());
                    LOGGER.info("Token sent to client");
				} catch (NothingException e) { 
					out.write("FAILED");
                    LOGGER.warning("Registration request failed " + e);
		    		System.err.println("Registration request failed.");
				}
	    		out.newLine();
	    		out.flush();

	    		return;
	    	}
	    	if(requestType.equals("LOGIN")) {
                System.out.println("Handling login request");
                LOGGER.info("Handling login request");
	    		user = handleLogin(in);
	    		try {
					out.write("LOGGED IN " + user.force().getValue().getName());
					out.newLine();
                    LOGGER.info("Logged in user ");
					out.write(user.force().getValue().getSession().stringRepresentation());
					LOGGER.info("Sent session token to client");
				} catch (NothingException e) {
					out.write("FAILED");
					LOGGER.warning("Login failed " + e);
				}
	    		out.newLine();
	    		out.flush();
	    		return;
	    	}
	    	if(requestType.equals("SEND MESSAGE")) {
                System.out.println("Handling message send request");
                LOGGER.info("Handling message send request");
	    		try {
					final Maybe<Message> message = handleMessage(user.force().getValue(),in);

					if(Server.sendMessage(user.force(),message.force())) {

						out.write("MESSAGE SENT");
						LOGGER.info("Message sent");

					} else {

						out.write("FAILED");
						LOGGER.warning("Message sending failed, server responded with false");

					}
				} catch (NothingException e) {
					out.write("FAILED");
					LOGGER.warning("Message sending failed " + e);
				}
	    		out.newLine();
	    		out.flush();
	    		return;
	    	}
	    	if(requestType.equals("READ MESSAGES")) {
	    	    LOGGER.info("Handling read message request");
	    		System.err.println("Handling a read message request");
	    		try {
	    			// Refresh the user object in order to get new messages.
		    		user = Server.refresh(user.force());
		    		LOGGER.info("Refreshed user");
	    			for (Message m : user.force().getValue().getMessages()) {
	    				System.err.println("Sending message from " + m.recipient);
	    				out.write("MESSAGE FROM " + m.recipient); out.newLine();
	    				out.write(m.message);out.newLine();
	    				out.write(".");out.newLine();
	    				out.flush();
	    				LOGGER.info("Sent a message to client");
	    			}
	    			out.write("END OF MESSAGES");
	    			LOGGER.info("Sent all messages");
	    			
				} catch (NothingException e) {
	    		    LOGGER.warning("Failed to send messages " + e);
					out.write("FAILED");
				}
	    		out.newLine();
	    		out.flush();
	    	}
	    }
		
		/**
		 * Handle a message send request
		 * @param user The User sending the message.
		 * @param in Reader to read the message data from.
		 * @return Message object.
		 */
		private static Maybe<Message> handleMessage(User user, BufferedReader in) throws IOException{
			final String lineOne = Util.getLine(in);
			final String lineTwo = Util.getLine(in);
			final String lineThree = Util.getLine(in);

			LOGGER.info("Handling a message");

			if (
				lineOne.startsWith("RECIPIENT")
				&& lineTwo.startsWith("TIMESTAMP")
				&& lineThree.startsWith("CONTENT")
			){
			    String cont = lineThree.substring("CONTENT ".length());
                byte[] deco = Base64.getDecoder().decode(cont);

                final Maybe<String> recipient = Server.validateRecipient(lineOne.substring("RECIPIENT ".length()));
				final Maybe<Date> timestamp = handleTimestamp(lineTwo.substring("TIMESTAMP ".length()));
				final Maybe<String> content = Maybe.just(new String(deco));

				try {
					Message msg = new Message(user, recipient.force(), timestamp.force(), content.force());
					LOGGER.info("Message handled, constructed and returning");
					return Maybe.just(msg);
				} catch (Message.Invalid | NothingException e) {
				    LOGGER.warning("Error handling message " + e);
					e.printStackTrace();
				}
			}
            LOGGER.info("Returned nothing while handling message");
			return Maybe.nothing();
		}

		private static Maybe<Date> handleTimestamp(String substring) {
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm");
			try {
				return Maybe.just(dateFormat.parse(substring));
				// convert to sql timestamp
				//     Timestamp timestamp = new java.sql.Timestamp(parsedDate.getTime());
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return Maybe.nothing();
		}


		/**
		 * Handle a registration request.
		 * @param in Request input.
		 * @return The stored user as a result of the registration.
		 * @throws IOException If the client hangs up unexpectedly.
		 */
		private static Maybe<Stored<User>> handleRegistration(BufferedReader in) throws IOException {
			final String lineOne = Util.getLine(in);
		    final String lineTwo = Util.getLine(in);

		    if (lineOne.startsWith("USER ") && lineTwo.startsWith("PASS ")) {

				try {
					final Username username = new Username(lineOne.substring("USER ".length()));
					final Hash hash = new Hash(lineTwo.substring("PASS ".length()));
					return Server.register(username, hash);
				} catch (IllegalUsername | IllegalPassword e) {
					return Maybe.nothing();
				}
			} else {
				return Maybe.nothing();
			}
			
		}
		
		/**
		 * Handle a login request.
		 * @param in Request input.
		 * @return User object as a result of a successful login.
		 * @throws IOException If the user hangs up unexpectedly.
		 */
		private static Maybe<Stored<User>> handleLogin(final BufferedReader in) throws IOException {

			final String lineOne = Util.getLine(in);
		    final String lineTwo = Util.getLine(in);
		    if (lineOne.startsWith("USER ") && lineTwo.startsWith("PASS ")) {

				try {
					final Username username = new Username(lineOne.substring("USER ".length()));
					final Maybe<String> password = Maybe.just(lineTwo.substring("PASS ".length()));
					System.err.println("Login request from user: " + username.getUsername());

					final String salt = UserBase.getSalt(username.getUsername());
					final Hash hash = new Hash(password.force(), salt);

					return Server.authenticate(username, hash);
				} catch (NothingException | IllegalUsername e) {
					return Maybe.nothing();
				} catch (IllegalPassword illegalPassword) {
                    illegalPassword.printStackTrace();
                }
            } else if(lineOne.startsWith("USER ") && lineTwo.startsWith("TOKEN ")) {

                try {
                    final Username username = new Username(lineOne.substring("USER ".length()));
                    final Maybe<String> token = Maybe.just(lineTwo.substring("TOKEN ".length()));
                    System.out.println("Login request from user: " + username.getUsername());
                    Token tokenObj = Token.fromBase(token.force());
                    return Server.authenticate(username, tokenObj);
                } catch (NothingException | IllegalUsername e) {
                    return Maybe.nothing();
                }
            }

			return Maybe.nothing();

		}
	}
}
