package inf226;

import inf226.exceptions.IllegalUsername;

final public class Username {
    private final String name;

    public Username(final String s) throws IllegalUsername{
        validate(s);
        this.name = s;
    }

    private static void validate(final String s) throws IllegalUsername{
        if (s.length() <= 4 || s.length() >= 32) throw new IllegalUsername();
        String pattern = "[a-zA-Z0-9]{4,32}";
        boolean match = s.matches(pattern);
        if (!match) throw new IllegalUsername();

    }

    public String getUsername(){
        return this.name;
    }
}
