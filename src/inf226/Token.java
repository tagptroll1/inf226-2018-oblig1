package inf226;

import java.util.Base64;
import java.util.Date;
import java.util.UUID;

public final class Token{
	private final UUID token;
	private final Date created;
	private Date lastUsed;

	protected Token() {
		token = getUUID();
		created = new Date();
		lastUsed = created;
	}

	private Token(UUID uuid){
		token = uuid;
		created = null;
		lastUsed = new Date();
	}

	static Token fromBase(String base){
		String decoded = new String(Base64.getDecoder().decode(base));
		return new Token(UUID.fromString(decoded));
	}

	Date getCreated() {
		return (Date)created.clone();
	}

	void setLastUsed(Date lastUsed) {
		this.lastUsed = lastUsed;
	}

	Date getLastUsed(){
		return (Date)lastUsed.clone();
	}

	private static UUID getUUID() {
		return UUID.randomUUID();
	}

	/**
	 * This method should return the Base64 encoding of the token
	 * @return A Base64 encoding of the token
	 */
	String stringRepresentation() {
		byte[] bytes = this.token.toString().getBytes();
		return Base64.getEncoder().encodeToString(bytes);
	}
}
